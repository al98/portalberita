<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

use App\Berita;

class BeritaController extends Controller
{
    public static function uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
          // 32 bits for "time_low"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff),
          // 16 bits for "time_mid"
          mt_rand(0, 0xffff),
          // 16 bits for "time_hi_and_version",
          // four most significant bits holds version number 4
          mt_rand(0, 0x0fff) | 0x4000,
          // 16 bits, 8 bits for "clk_seq_hi_res",
          // 8 bits for "clk_seq_low",
          // two most significant bits holds zero and one for variant DCE1.1
          mt_rand(0, 0x3fff) | 0x8000,
          // 48 bits for "node"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function index()
    {
        $berita = Berita::select([
                            'id as id_berita',
                            'judul',
                            'isi_berita',
                            'gambar',
                            'penulis'
                        ])
                        ->paginate(16);

    	$breadcrumb = [
    		'berita' => route('backend.berita.index')
    	];

    	return view('backend.pages.berita.index',[
    		'pageTitle' => 'Cpanel - Portal Berita',
    		'breadcrumb' => $breadcrumb,
            'berita' => $berita
    	]);
    }

    public function create()
    {
    	$breadcrumb = [
    		'berita' => route('backend.berita.index'),
    		'tambah' => route('backend.berita.create')
    	];

    	return view('backend.pages.berita.create',[
    		'pageTitle' => 'Tambah Berita',
    		'breadcrumb' => $breadcrumb
    	]);
    }

    public function store(Request $request)
    {
    	$request->validate([
            'gambar' => 'required|image',
            'judul'  => 'required|max:255',
            'isi'  => 'required',
            'penulis'  => 'required|max:60',
        ]);

        try{
            DB::transaction(function() use($request){ 
                $berita = new Berita;
                $berita->id = $this->uuid();
                $berita->judul = $request->judul;
                $berita->isi_berita = $request->isi;
                $berita->penulis = $request->penulis;

                if($request->hasFile('gambar') ){
                    if($request->file('gambar')->isValid() ){
                        $file = str_slug('Berita-'.str_random(20)).$request->file('gambar')->getClientOriginalName();
                        $berita->gambar = $file;
                        $dir = './upload/files/img/berita/';
                        $request->file('gambar')->move($dir,$file);
                    }
                }
                $berita->save();
            });

            $messages = '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              Berita Berhasil ditambah !.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
            return back()
                    ->withMessages($messages);
        }catch(Exception $e){
            abort(500);
        }
    }

    public function edit($id)
    {
        $berita = Berita::select([
                            'id as id_berita',
                            'judul',
                            'isi_berita',
                            'gambar',
                            'penulis'
                        ])
                        ->where([
                            'id' => $id
                        ])
                        ->first();

        $breadcrumb = [
            'berita' => route('backend.berita.index'),
            'edit' => route('backend.berita.edit',$id),
            $berita->judul => ''
        ];

    	return view('backend.pages.berita.edit',[
    		'pageTitle' => 'Cpanel - Portal Berita',
            'breadcrumb' => $breadcrumb,
            'berita' => $berita
    	]);
    }

    public function update(Request $request,$id)
    {
    	$request->validate([
            'judul'  => 'required|max:255',
            'isi'  => 'required',
            'penulis'  => 'required|max:60',
        ]);

        try{
            DB::transaction(function() use($request,$id){ 
                $berita = Berita::where([
                        'id' => $id
                    ])
                    ->first();

                $berita->judul = $request->judul;
                $berita->isi_berita = $request->isi;
                $berita->penulis = $request->penulis;

                if($request->hasFile('gambar')){
                    if($request->File('gambar')->isValid()){

                        $image = $berita->gambar;
                        $dir = './upload/files/img/berita/';
                        if(file_exists($dir.$image)){
                            unlink($dir.$image);
                        }
                        
                        $file = str_slug('Berita-'.str_random(5)).$request->file('gambar')->getClientOriginalName();
                        $berita->gambar = $file;
                        $request->file('gambar')->move($dir,$file);
                    }
                }

                $berita->save();
            });

            $messages = '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              Berita Berhasil diupdate !;
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
            return back()
                    ->withMessages($messages);
        }catch(Exception $e){
            abort(500);
        }
    }

    public function destroy($id)
    {
        $berita = Berita::where([
                        'id' => $id
                    ])
                    ->first();

        $image = $berita->gambar;
        $dir = './upload/files/img/berita/';
        if(file_exists($dir.$image) && $image !=null){
            unlink($dir.$image);
        }        
        $berita->delete();

        $messages = '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              Berita Berhasil dihapus !
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        return back()
                ->withMessages($messages);
    }
}
