<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function getDashboard()
    {
    	return view('backend.pages.app.dashboard',[
    		'pageTitle' => 'Cpanel - Portal Berita'
    	]);
    }
}
