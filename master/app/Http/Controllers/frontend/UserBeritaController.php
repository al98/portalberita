<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Berita;

class UserBeritaController extends Controller
{
    public function getIndex()
    {
    	$berita = Berita::select([
    							'id as id_berita',
    							'judul',
    							'gambar',
    							'isi_berita',
    							'created_at as tanggal'
    						])
    						->paginate(15);

    	return view('frontend.pages.berita.index',[
    		'pageTitle' => 'Portal Berita',
            'berita' => $berita
    	]);
    }

    public function getSingle($id)
    {
    	$berita = Berita::select([
    							'id as id_berita',
    							'judul',
    							'gambar',
    							'isi_berita',
    							'created_at as tanggal'
    						])
    						->where([
    							'id' => $id
    						])
    						->first();

    	return view('frontend.pages.berita.single',[
    		'pageTitle' => 'Portal Berita - '.$berita->judul,
            'berita' => $berita
    	]);
    }
}
