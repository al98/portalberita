<?php

Route::namespace('backend')->prefix('cpanel')->group(function(){
	Route::get('/', 'AuthController@getDashboard')->name('backend.dashboard');
	Route::resource('berita', 'BeritaController',['as' => 'backend']);
});

Route::namespace('frontend')->group(function(){
	Route::get('/','UserBeritaController@getIndex')->name('frontend.index');
	Route::get('berita/{id}','UserBeritaController@getSingle')->name('frontend.berita.details');
});