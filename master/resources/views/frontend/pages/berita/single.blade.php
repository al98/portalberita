@extends('frontend.layouts.app')

@section('content')
<div class="container-fluid py-4">
	<div class="container">
		<h4>{{ $berita->judul }}</h4>
		<p class="text-muted text-italic">{{ $berita->tanggal }}</p>
		<img class="img-fluid" src="{{ asset('upload/files/img/berita/'.$berita->gambar) }}">
		<div class="description mt-3 p-2 border">
			{!! $berita->isi_berita !!}
		</div>
	</div>
</div>
@endsection