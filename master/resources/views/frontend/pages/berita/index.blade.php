@extends('frontend.layouts.app')

@section('content')
<div class="container-fluid py-4">
	<div class="container">
		<h4 class="pb-3">Daftar Berita</h4>

		<div>
			<ul class="list-unstyled">
				@forelse($berita as $b)
				<li class="media border p-2 mb-2">
					<img class="mr-3 img-fluid w-50" src="{{ asset('upload/files/img/berita/'.$b->gambar) }}">
					<div class="media-body">
						<h5 class="mt-0 mb-1"> {{ $b->judul }} </h5>
						<p class="text-muted text-italic">{{ $b->tanggal }}</p>
						{!! substr($b->isi_berita, 0,400) . '...' !!}
						<div class="text-right">
							<a href="{{ route('frontend.berita.details',$b->id_berita) }}"> Baca selengkapnya </a>
						</div>
					</div>
				</li>
				@empty
				<li class="media">
					<img class="mr-3" src="">
					<div class="media-body">
						<h5 class="mt-0 mb-1"> Data Kosong </h5>
					</div>
				</li>
				@endforelse
			</ul>
		</div>
	</div>
</div>
@endsection