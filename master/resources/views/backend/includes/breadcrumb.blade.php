<div class="container px-0 py-2">
	<div class="row">
		@if(isset($breadcrumb))
		<div class="col-md-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item" aria-current="page"><a href="{{ route('backend.dashboard') }}">Dashboard</a></li>
					@foreach($breadcrumb as $key=>$val)
					<li class="breadcrumb-item" aria-current="page"><a href="{{ $val }}">{{ $key }}</a></li>
					@endforeach
				</ol>
			</nav>
		</div>
		@endif
	</div>
</div>
