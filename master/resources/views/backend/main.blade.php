<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Alfeus Adi Saputra">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title> {{ @$pageTitle }} </title>

	<link rel="stylesheet" type="text/css" href="{{ asset('themes/assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('themes/assets/css/style.css') }}">

	@stack('head')

</head>
<body>
	
	@yield('body')

<script type="text/javascript" src="{{ asset('themes/assets/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/assets/js/bootstrap.min.js') }}"></script>
@stack('js')

</body>
</html>