@extends('backend.main')

@section('body')

<div class="header">
	<div class="navbar navbar-expand-lg navbar-light">
		<div class="container">
			<a class="navbar-brand" href="{{ route('frontend.index') }}">
				<h4 class="text-primary">Portal Berita</h4>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNavbar" aria-controls="headerNavbar" aria-expanded="true" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="headerNavbar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="#">
							Admin
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="body">
	@yield('content')
</div>

<div class="footer">
	<div class="container py-4">
		<h6 class="py-0 text-center text-white"> &copy; 2019. developed by alfeus adi saputra</h6>
	</div>
</div>

@endsection