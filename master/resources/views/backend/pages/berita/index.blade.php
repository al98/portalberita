@extends('backend.layouts.app')

@push('js')
<script type="text/javascript">
	$(".btn-delete").click(function(){
		var judul = $(this).attr("data-judul");
		var id = $(this).attr("data-id");

		$("#judul_berita").html(judul);
		$("#btn_confirm").attr("data-id",id);
		$("#modal_delete").modal();
	});

	$("#btn_confirm").click(function(){
		var id = $(this).attr("data-id");
		if($(".delete-"+id).submit()){
			$("#modal_delete").modal('hide');
		}
	});

</script>
@endpush

@section('content')
<div class="container-fluid py-5">
	<div class="container">
		<h4 class="text-primary"> Dashboard </h4>

		@include('backend.includes.breadcrumb')
		@include('backend.includes.errors')
		<div class="text-right">
			<a href="{{ route('backend.berita.create') }}" class="btn btn-md btn-success mb-2">
				<i class="fas fa-plus-circle text-white"></i>
				Tambah
			</a>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th width="20%">Gambar</th>
					<th>Judul</th>
					<th>Penulis</th>
					<th width="15%">Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($berita as $b)
				<tr>
					<td>
						<img src="{{ asset('upload/files/img/berita/'.$b->gambar) }}" class="img-fluid">
					</td>
					<td>{{ $b->judul }}</td>
					<td>{{ $b->penulis }}</td>
					<td>
						<a href="{{ route('backend.berita.edit',$b->id_berita) }}" class="btn btn-sm btn-success">
							<i class="fas fa-pencil-alt mr-2 text-white"></i>
							edit
						</a>
						<form class="delete-{{ $b->id_berita }}" action="{{ route('backend.berita.destroy', $b->id_berita) }}" method="POST">
							{!! csrf_field() !!}
							{!! method_field('DELETE') !!}
				       	 	<button type="button" class="btn-delete btn btn-danger btn-sm mt-2" data-judul="{{ $b->judul }}" data-id="{{ $b->id_berita }}">
				       	 		<i class="fas fa-trash"></i>
				       	 		hapus
				       	 	</button>
				    	</form>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="4"> <h5>Berita Kosong</h5></td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data <span id="judul_berita"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>
      		Apakah anda yakin ingin menghapus data ini ?
      	</p>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="button" id="btn_confirm" class="btn btn-success px-4" data-id="">Ya</button>
      </div>
    </div>
  </div>
</div>
@endsection