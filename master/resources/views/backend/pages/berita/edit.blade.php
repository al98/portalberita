@extends('backend.layouts.app')

@push('js')
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
@endpush

@section('content')
<div class="container-fluid py-5">
	<div class="container">
		<h4 class="text-primary"> Edit Berita </h4>
		@include('backend.includes.breadcrumb')		

		<div>
		<div class="container border py-3">
			<form action="{{ route('backend.berita.update',$berita->id_berita) }}" method="post" enctype="multipart/form-data">
				{!! csrf_field() !!}
				{!! method_field('PUT') !!}
				<div class="form-group">
					<label for="gambar" class="font-weight-bold">Upload Gambar</label>
					<div class="col-6 px-0">
						<input type="file" name="gambar" id="gambar" class="p-2 border">
						<div class="py-1">
							<img src="{{ asset('Upload/files/img/berita/'.$berita->gambar) }}" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="judul" class="font-weight-bold">Judul Berita</label>
					<input type="text" name="judul" value="{{ old('judul',$berita->judul) }}" placeholder="Judul Berita" class="form-control form-control-md py-3">
				</div>
				<div class="form-group">
					<label for="penulis" class="font-weight-bold">Penulis</label>
					<div class="col-4 px-0">
						<select name="penulis" id="penulis" class="form-control form-control-md">
							<option>-- Penulis --</option>
							<option value="Andi" {{ $berita->penulis == 'Andi' ? 'selected' : '' }} >Andi</option>
							<option value="Tomi" {{ $berita->penulis == 'Tomi' ? 'selected' : '' }} >Tomi</option>
							<option value="Andriyani" {{ $berita->penulis == 'Andriyani' ? 'selected' : '' }} >Andriyani</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="isi" class="font-weight-bold">Isi Berita</label>
					<textarea name="isi" id="isi" placeholder="isi berita ... ">{{ $berita->isi_berita }}</textarea>
				</div>
				<div class="text-right pt-3">
					<button type="submit" class="btn btn-md btn-primary">
						<i class="fas fa-save"></i>
						update
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection