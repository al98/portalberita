@extends('backend.layouts.app')

@section('content')
<div class="container-fluid py-5">
	<div class="container">
		<h4 class="text-primary"> Dashboard </h4>

		<div class="row">
			<div class="col-6 col-md-4 mb-2">
				<div class="card">
					<div class="text-center">
						<i class="far fa-newspaper fa-7x"></i>
					</div>
					<div class="card-body p-2 bg-secondary">
						<h5 class="text-center my-0">
							<a href="{{ route('backend.berita.index') }}" class="text-white">
								Berita
							</a>
						</h5>
					</div>
				</div>
			</div>
		</div><!-- End of row -->
	</div>
</div>
@endsection